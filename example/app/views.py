from .models import ExampleModel
from .forms import ExampleModelForm
from django.views.generic import CreateView


class ExampleModelView(CreateView):
    form_class = ExampleModelForm
    model = ExampleModel
    template_name = 'exampleview.html'